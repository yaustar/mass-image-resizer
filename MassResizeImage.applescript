set image_ext_list to {"jpg", "jpeg", "png"}
set source_folder_name to choose folder "Choose folder with source images" without invisibles

with timeout of 86400 seconds
	tell application "Finder"
		set image_files to (files of entire contents of source_folder_name whose name extension is in image_ext_list) as alias list
	end tell
end timeout

set the target_length to 1080

repeat with image_file in image_files
	try
		with timeout of 86400 seconds
			tell application "Image Events"
				-- start the Image Events application
				launch
				
				-- open the image file
				set this_image to open image_file
				
				-- get dimensions of the image
				copy dimensions of this_image to {W, H}
				
				-- determine the shortest side and then
				-- calculate the new length for the longer side
				if W is less than H then
					set the scale_length to (H * target_length) / W
					set the scale_length to �
						round scale_length rounding as taught in school
				else
					set the scale_length to (W * target_length) / H
					set the scale_length to �
						round scale_length rounding as taught in school
				end if
				
				set should_we_scale_image to false
				if W is greater than target_length then
					set should_we_scale_image to true
				end if
				
				if H is greater than target_length then
					set should_we_scale_image to true
				end if
				
				if should_we_scale_image then
					-- perform action
					scale this_image to size scale_length
					-- save the changes
					save this_image with icon
				end if
				
				-- purge the open image data
				close this_image
			end tell
		end timeout
		
	on error error_message
		display dialog error_message
	end try
end repeat

display dialog "Finished"